/*var bitcore = require('bitcore-lib');

var privateKey = new bitcore.PrivateKey();
var address = privateKey.toAddress();
console.log(privateKey, typeof privateKey, privateKey.PrivateKey, address, typeof address, address.Address);
console.log(privateKey.toString(), address.toString());    
*/

var ethers = require('ethers');

let infuraProvider = new ethers.providers.InfuraProvider('ropsten');

let randomWallet = ethers.Wallet.createRandom(infuraProvider);

console.log(randomWallet.address, randomWallet.privateKey);


