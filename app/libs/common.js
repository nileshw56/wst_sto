var bitcore = require('bitcore-lib');
var ethers = require('ethers');


module.exports.generateBTCAddress = function() {
    var privateKey = new bitcore.PrivateKey();
    var address = privateKey.toAddress();

    return [address.toString(), privateKey.toString()];
};

module.exports.generateETHAddress = function() {
    let infuraProvider = new ethers.providers.InfuraProvider(app.config.etherNet);
    let randomWallet = ethers.Wallet.createRandom(infuraProvider);
    return [randomWallet.address, randomWallet.privateKey];
};

module.exports.generateAddress = function(symbol) {
    
    var address;
    switch(symbol) {
        
        case 'BTC' :
            address = module.exports.generateBTCAddress();
            break;
        
        case 'ETH' :
            address = module.exports.generateETHAddress();
            break; 
    }

    return address;
}