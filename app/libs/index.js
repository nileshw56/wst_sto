
module.exports = function() {

    app.lib = {};

    app.lib._ = require("lodash");
    app.lib.util = require("util");
    app.lib.async = require('async');
    app.lib.request = require('request');
    app.lib.mailer = require('./mailer')
    //app.lib.api = require('./api');
    app.lib.common = require('./common');

}